package com.yusufelosman.ecodation;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.yusufelosman.ecodation.model.UserDTO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class EcodationTeknikTaskApplicationTests {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void successTest() throws Exception {
		UserDTO userDTO = new UserDTO();
		userDTO.setName("yusuf");
		userDTO.setSurname("elosman");
		userDTO.setEmail("yusuf_elosman@hotmail.com");
		userDTO.setPassword("12345678");

		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
		ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
		String requestJson=ow.writeValueAsString(userDTO);

		this.mockMvc.perform(post("/user/adduser").contentType(APPLICATION_JSON_UTF8)
		.content(requestJson)).andDo(print()).andExpect(status().isOk());
    }

	@Test
	public void passwordLengthFailTest() throws Exception {
		UserDTO userDTO = new UserDTO();
		userDTO.setName("yusuf");
		userDTO.setSurname("elosman");
		userDTO.setEmail("yusuf_elosman@hotmail.com");
		userDTO.setPassword("12");

		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
		ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
		String requestJson=ow.writeValueAsString(userDTO);

		this.mockMvc.perform(post("/user/adduser").contentType(APPLICATION_JSON_UTF8)
				.content(requestJson)).andDo(print()).andExpect(status().isOk());
	}
}
