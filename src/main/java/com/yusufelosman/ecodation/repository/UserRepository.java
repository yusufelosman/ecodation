package com.yusufelosman.ecodation.repository;

import com.yusufelosman.ecodation.entity.UserEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends CrudRepository<UserEntity, Long> {
    List<UserEntity> fetchAllUser();

    List<UserEntity> getUsersByName(String name);

    List<UserEntity> getUsersBySurname(String surname);

    List<UserEntity> getUsersByEmail(String email);
}
