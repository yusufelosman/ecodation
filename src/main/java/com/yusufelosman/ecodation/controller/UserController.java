package com.yusufelosman.ecodation.controller;

import com.yusufelosman.ecodation.service.UserService;
import com.yusufelosman.ecodation.model.UserDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/user/")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping("/adduser")
    public ResponseEntity<?> addUser(@Valid @RequestBody UserDTO userRequest) {
        userService.addUser(userRequest);
        return ResponseEntity.ok("User " + userRequest.getName() + " saved");
    }

    @GetMapping("/getAllUser")
    public ResponseEntity<?> getAllUser() {
        List<UserDTO> userDTOList =  userService.getAllUser();
        return ResponseEntity.ok(userDTOList);
    }

    @GetMapping("/getUsersByName/{name}")
    public ResponseEntity<?> getUsersByName(@PathVariable String name) {
        List<UserDTO> userDTOList =  userService.getUsersByName(name);
        return ResponseEntity.ok(userDTOList);
    }

    @GetMapping("/getUsersBySurname/{surname}")
    public ResponseEntity<?> getUsersBySurname(@PathVariable String surname) {
        List<UserDTO> userDTOList =  userService.getUsersBySurname(surname);
        return ResponseEntity.ok(userDTOList);
    }

    @GetMapping("/getUsersByEmail/{email}")
    public ResponseEntity<?> getUsersByEmail(@PathVariable String email) {
        List<UserDTO> userDTOList =  userService.getUsersByEmail(email);
        return ResponseEntity.ok(userDTOList);
    }

}
