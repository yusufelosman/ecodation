package com.yusufelosman.ecodation.service;

import com.yusufelosman.ecodation.entity.UserEntity;
import com.yusufelosman.ecodation.model.UserDTO;
import com.yusufelosman.ecodation.repository.UserRepository;
import lombok.SneakyThrows;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserService {

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder encoder;

    @SneakyThrows
    public void addUser(UserDTO userRequest) {
        UserEntity entity = modelMapper.map(userRequest, UserEntity.class);
        String encodePassword = encoder.encode(entity.getPassword());
        entity.setPassword(encodePassword);
        userRepository.save(entity);
    }

    public List<UserDTO> getAllUser() {
        List<UserEntity> userEntityList = userRepository.fetchAllUser();
        return userEntityList.stream().map(userEntity -> modelMapper.map(userEntity, UserDTO.class)).collect(Collectors.toList());
    }

    public List<UserDTO> getUsersByName(String name) {
        List<UserEntity> userEntityList = userRepository.getUsersByName(name);
        return userEntityList.stream().map(userEntity -> modelMapper.map(userEntity, UserDTO.class)).collect(Collectors.toList());
    }

    public List<UserDTO> getUsersBySurname(String surname) {
        List<UserEntity> userEntityList = userRepository.getUsersBySurname(surname);
        return userEntityList.stream().map(userEntity -> modelMapper.map(userEntity, UserDTO.class)).collect(Collectors.toList());
    }

    public List<UserDTO> getUsersByEmail(String email) {
        List<UserEntity> userEntityList = userRepository.getUsersByEmail(email);
        return userEntityList.stream().map(userEntity -> modelMapper.map(userEntity, UserDTO.class)).collect(Collectors.toList());
    }
}
