package com.yusufelosman.ecodation.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Entity
@Table(name = "users")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@NamedQueries({
        @NamedQuery(name = "UserEntity.fetchAllUser",
                query = "SELECT u FROM UserEntity u"),
        @NamedQuery(name = "UserEntity.getUsersByName",
                query = "SELECT u FROM UserEntity u where u.name = ?1"),
        @NamedQuery(name = "UserEntity.getUsersBySurname",
                query = "SELECT u FROM UserEntity u where u.surname = ?1"),
        @NamedQuery(name = "UserEntity.getUsersByEmail",
                query = "SELECT u FROM UserEntity u where u.email = ?1")
})
public class UserEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    @Size(max = 50)
    private String name;

    @NotBlank
    @Size(max = 50)
    private String surname;

    @NotBlank
    @Size(max = 50)
    private String email;

    @NotBlank
    @Size(min = 8)
    private String password;
}
